from sklearn.metrics.pairwise import cosine_similarity
import numpy as np


class PageComparator:

    def compare(self, words_counters):
        words_warehouse = self.__create_all_words_warehouse(words_counters)
        pages = self.__compare_word_warehouse(words_counters, words_warehouse)
        result = cosine_similarity(np.array(pages))
        return result

    def __create_all_words_warehouse(self, words_counters):
        words_warehouse = set()
        for page_no in range(len(words_counters)):
            words_warehouse.update(words_counters[page_no])
        return words_warehouse

    def __compare_word_warehouse(self, words_counters, words_warehouse):
        pages = []
        for page_no in range(len(words_counters)):
            page = []
            for element in sorted(words_warehouse):
                if element in words_counters[page_no]:
                    page.append(words_counters[page_no][element])
                else:
                    page.append(0)
            pages.append(page)
        return pages
