from urllib.request import Request, urlopen
from urllib.error import HTTPError, URLError
from bs4 import BeautifulSoup
from collections import Counter
import validators
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import ssl
import re


class PageExplorer:

    def __init__(self, url, depth, text_flag=False, link_flag=False, script_flag=False, image_flag=False):

        self._words = Counter()
        self._links = set()
        self._scripts = set()
        self._images = set()
        self._urls = set()

        self._urls.add(url)
        self._general_url = url
        self._depth = depth
        self._text_flag = text_flag
        self._link_flag = link_flag
        self._script_flag = script_flag
        self._image_flag = image_flag

        self._source_nodes = []
        self._target_nodes = []

    def execute_page_walk(self):
        self.__walk_page(self._general_url, self._depth)
        self.__print_graph()

    def __walk_page(self, current_url, current_depth):
        print('CURRENT URL:', current_url)
        print('CURRENT DEPTH:', current_depth)
        if self._text_flag:
            self.__get_words(current_url)
        if self._link_flag:
            self.__get_links(current_url)
        if self._script_flag:
            self.__get_scripts(current_url)
        if self._image_flag:
            self.__get_images(current_url)

        # if current_depth > 1:
        #     for url in self.__get_link_generator(current_url):
        #         if url not in self._urls and '.exe' not in url:
        #             self._urls.add(url)
        #             self.__walk_page(url, current_depth - 1)

        if current_depth > 1:
            for url in self.__get_link_generator(current_url):
                if '.exe' not in url:
                    if self.__make_raw_url(url) not in self._urls:
                        self._urls.add(self.__make_raw_url(url))
                        self.__walk_page(url, current_depth - 1)
                    self._source_nodes.append(self.__make_raw_url(current_url))
                    self._target_nodes.append(self.__make_raw_url(url))
        elif current_depth is 1:
            for url in self.__get_link_generator(current_url):
                if '.exe' not in url:
                    if self.__make_raw_url(url) in self._urls:
                        self._source_nodes.append(self.__make_raw_url(current_url))
                        self._target_nodes.append(self.__make_raw_url(url))

        # if current_depth > 1:
        #     for url in self.__get_link_generator(current_url):
        #         if '.exe' not in url:
        #             if url not in self._urls:
        #                 self._urls.add(url)
        #                 self.__walk_page(url, current_depth - 1)
        #             self._source_nodes.append(self.__make_raw_url(current_url))
        #             self._target_nodes.append(self.__make_raw_url(url))
        # elif current_depth is 1:
        #     for url in self.__get_link_generator(current_url):
        #         if '.exe' not in url:
        #             if url in self._urls:
        #                 self._source_nodes.append(self.__make_raw_url(current_url))
        #                 self._target_nodes.append(self.__make_raw_url(url))


    def __load_page(self, url):
        req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        with urlopen(req, context=ssl._create_unverified_context()) as context:
            soup = BeautifulSoup(context, 'html.parser')
        return soup

    def __get_link_generator(self, url):
        try:
            for tag in self.__load_page(url).findAll('a'):
                try:
                    if validators.url(tag['href']):
                        yield tag['href']
                except KeyError:
                    pass
        except (HTTPError, URLError, UnicodeEncodeError, TimeoutError):
            pass

    def __get_words(self, url):
        try:
            soup = self.__load_page(url)
            for script in soup(['script', 'style']):
                script.extract()
            text = list(filter(None, re.sub('[\s\-–=&:,!|„”"\'`@\?;/\$%\\\(\)\{\}\[\]\.\+\*0-9]+', ' ',
                                            soup.get_text()).lower().split(' ')))
            self.words.update(Counter(text))
        except (HTTPError, URLError, UnicodeEncodeError, TimeoutError):
            pass

    def __get_links(self, url):
        try:
            for tag in self.__load_page(url).findAll('a'):
                try:
                    link = tag['href']
                    if validators.url(link):
                        self._links.add(link)
                except KeyError:
                    pass
        except (HTTPError, URLError, UnicodeEncodeError, TimeoutError):
            pass

    def __get_scripts(self, url):
        try:
            for tag in self.__load_page(url).findAll('script'):
                try:
                    self._scripts.add(tag.text)
                except KeyError:
                    pass
        except (HTTPError, URLError, UnicodeEncodeError, TimeoutError):
            pass

    def __get_images(self, url):
        try:
            for tag in self.__load_page(url).findAll('img'):
                try:
                    self._images.add(tag['src'])
                except KeyError:
                    pass
        except (HTTPError, URLError, UnicodeEncodeError, TimeoutError):
            pass

    def __print_graph(self):
        df = pd.DataFrame({'from': self._source_nodes, 'to': self._target_nodes})
        graph = nx.from_pandas_edgelist(df, 'from', 'to', create_using=nx.DiGraph())
        pr = nx.pagerank(graph, alpha=0.85)
        pages = list(pr.keys())
        ranks = list(pr.values())
        carac = pd.DataFrame({'page': pages, 'rank': ranks})
        graph.nodes()
        carac = carac.set_index('page')
        carac = carac.reindex(graph.nodes())
        nx.draw(graph, with_labels=True, arrows=True, alpha=0.85, node_color=carac['rank'],
                cmap=plt.cm.Blues, node_size=750, linewidths=5)
        plt.show()
        print('PAGE RANK:', pr)

    def __make_raw_url(self, url):
        return re.sub('www.', '', str(url).split('//')[1].split('/')[0].split('?')[0])
        # return str(url)

    @property
    def words(self):
        return self._words

    @property
    def links(self):
        return self._links

    @property
    def scripts(self):
        return self._scripts

    @property
    def images(self):
        return self._images
