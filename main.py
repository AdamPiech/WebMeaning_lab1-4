from engine.page_explorer import PageExplorer
from engine.page_comparator import PageComparator
import codecs
import getopt
import sys


urls = []
words_counters = []
out_file = None
depth = 1

site_flag = False
file_flag = False
console_flag = False
text_flag = False
link_flag = False
script_flag = False
image_flag = False


def write(data):
    if console_flag:
        print(data)
    if file_flag:
        save_to_file(data)


def save_to_file(data):
    with codecs.open(out_file, 'a', encoding='utf8') as file:
        file.write(data + '\n')


def erase_file():
    with open(out_file, 'w+') as file:
        file.truncate()


try:
    opts, args = getopt.getopt(sys.argv[1:], 's:f:d:ctlji',
                               ['site=', 'file=', 'depth=', 'console', 'text', 'link', 'script', 'image'])
except getopt.GetoptError:
    print("ERROR: Invalid arguments!")
    sys.exit(2)

try:
    for opt, arg in opts:
        if opt in ('-s', '--site'):
            site_flag = True
            urls.append(arg)
        if opt in ('-f', '--file'):
            file_flag = True
            out_file = arg
            erase_file()
        if opt in ('-d', '--depth'):
            depth = int(arg)
        if opt in ('-c', '--console'):
            console_flag = True
        if opt in ('-t', '--text'):
            text_flag = True
        if opt in ('-l', '--link'):
            link_flag = True
        if opt in ('-j', '--script'):
            script_flag = True
        if opt in ('-i', '--image'):
            image_flag = True
except KeyError:
    pass


if site_flag:
    for url in urls:
        pe = PageExplorer(url, depth,
                          text_flag=text_flag, link_flag=link_flag, script_flag=script_flag, image_flag=image_flag)
        pe.execute_page_walk()

        write('PAGE: ' + url)
        if text_flag:
            write('WORDS:')
            words_counters.append(dict(pe.words.most_common()))
            for entry in pe.words.most_common():
                write(entry[0] + ': ' + str(entry[1]))
        if link_flag:
            write('LINKS:')
            for link in pe.links:
                write(link)
        if script_flag:
            write('SCRIPTS:')
            for script in pe.scripts:
                write(script)
        if image_flag:
            write('IMAGES:')
            for image in pe.images:
                write(image)

if text_flag and len(urls) > 1:
    pc = PageComparator()
    result = pc.compare(words_counters)
    print('SIMILARITY:')
    print(result)
